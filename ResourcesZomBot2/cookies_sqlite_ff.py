#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob
import time
import re
import sqlite3
import subprocess
from sett import Settings
sys.path.append(os.path.abspath('..\\API'))
import colorprint as cp
# import game_state.colorprint as cp
from vk_auth import VK

FIELDS = '(baseDomain, originAttributes, name, value, host, path, expiry, lastAccessed, creationTime, isSecure, isHttpOnly, appId, inBrowserElement)'
FIREFOX = os.getenv('PROGRAMFILES') + u'\\Mozilla Firefox\\firefox.exe'
GAME = u'https:\\\\vk.com\\app612925_' # 66156092


def pr(cur):
    for row in cur:
        # print row
        for t in row:
            print t, #.decode('utf-8')
        print

def pr_all():
    print
    cur.execute('SELECT * FROM classes')
    for row in cur:
        # print row
        for t in row:
            print t, #.decode('utf-8')
        print

def save_result(cur):
    with open('result.txt', 'w') as f:
        text = u''
        for row in cur:
            text += unicode(row) + u'\n'
        f.write(text.encode('utf-8'))

def get_path_firefox_cookies():
    # %APPDATA%\Mozilla\Firefox\Profiles
    # %USERPROFILE%
    path = os.getenv('APPDATA') + '\\Mozilla\\Firefox\\Profiles\\'
    path = path.decode('cp1251')
    folders = glob.glob(path + u'*.default')
    if not folders:
        cp.cprint(u'12Не найден файл cookies.sqlite!')
        cp.cprint (u'14-------------   ^12_ERROR   ^14_---------------')
        raw_input()
        exit(1)
    return folders[0] + '\\cookies.sqlite'

def check_param():
    if len(sys.argv) < 3:
        cp.cprint(u'12Недостаточно параметров!')
        cp.cprint (u'14-------------   ^12_ERROR   ^14_---------------')
        raw_input()
        exit(1)

def str2dict(val):
    if not ((type(val) is str) or (type(val) is unicode)): return val
    res = {tmp.split('=')[0]: tmp.split('=')[1] for tmp in val.replace(' ','').split('\n') if tmp}
    return res

def get_num_akk():
    if len(sys.argv) > 1:
        return int(sys.argv[1])
    return None

def get_auth_and_token(curuser):
    filename = '.\\cookies\\' + curuser + '_auth_and_token.txt'
    if not os.path.isfile(filename): return None
    with open(filename, 'r') as fr:
        text = fr.read().decode('utf-8')
        data = str2dict(text)
        return data

def get_user_id(curuser):
    auth_and_token = get_auth_and_token(curuser)
    if auth_and_token:
        return auth_and_token.get('user_id')
    return None

def get_path_ini():
    # print u'Текущая папка:', os.getcwd()
    # print u'Переданные параметры:\n', sys.argv, u'\n'
    if len(sys.argv) > 2:
        path_ini = unicode(sys.argv[2], 'cp1251')
        path_ini = os.path.split(path_ini)
        print u'Путь к settings.ini:', path_ini[0], path_ini[1], u'\n'
        return path_ini
    return None

def set_cookies_firefox(name_db, cookies):
    new_remixsid = cookies.get('remixsid')
    new_remixlang = cookies.get('remixlang')
    new_remixsslsid = cookies.get('remixsslsid')
    if not (cookies and new_remixsid and new_remixlang and new_remixsslsid):
        return
    print

    conn = sqlite3.connect(name_db)
    cur = conn.cursor()

    cur.execute('SELECT SQLITE_VERSION()')
    data = cur.fetchone()
    print 'SQLite version: %s' % data

    # все куки хоста .vk.com
    # cur.execute('SELECT * FROM moz_cookies WHERE host = ".vk.com"')
    # pr(cur)
    # save_result(cur)


    cur.execute('SELECT * FROM moz_cookies WHERE host = ".vk.com" AND name = "remixsid"')
    get_remixsid = cur.fetchone()
    print 'get_remixsid:', get_remixsid

    if get_remixsid:
        expiry = get_remixsid[7]
        expiry_new = int(time.time() + 31536000)
        cur.execute('UPDATE moz_cookies SET value = "%s", expiry = %d WHERE host = ".vk.com" AND name = "remixsid"' % (new_remixsid, expiry_new))
        print u'Обновляем cookie  remixsid'
        print 'UPDATE moz_cookies SET value = "%s", expiry = %d WHERE host = ".vk.com" AND name = "remixsid"' % (new_remixsid, expiry_new)
    else:
        cur.execute('INSERT INTO moz_cookies %s VALUES (%s?)' % (FIELDS, '?, '*12),
        (u'vk.com', u'', u'remixsid', new_remixsid, u'.vk.com', u'/', int(time.time()), int(time.time()*1000000), int(time.time()*1000000), 1, 0, 0, 0))
        print u'Добавляем cookie  remixsid'
    conn.commit()

    cur.execute('SELECT * FROM moz_cookies WHERE host = ".vk.com" AND name = "remixlang"')
    if cur is None:
        cur.execute('INSERT INTO moz_cookies %s VALUES (%s?)' % (FIELDS, '?, '*12),
        (u'vk.com', u'', u'remixlang', new_remixlang, u'.vk.com', u'/', int(time.time()), int(time.time()*1000000), int(time.time()*1000000), 0, 0, 0, 0))
        print u'Добавляем cookie  remixlang'
        conn.commit()

    cur.execute('SELECT * FROM moz_cookies WHERE host = ".vk.com" AND name = "remixsslsid"')
    if cur is None:
        cur.execute('INSERT INTO moz_cookies %s VALUES (%s?)' % (FIELDS, '?, '*12),
        (u'vk.com', u'', u'remixsslsid', new_remixsslsid, u'.vk.com', u'/', int(time.time()), int(time.time()*1000000), int(time.time()*1000000), 0, 0, 0, 0))
        print u'Добавляем cookie  remixsslsid'
        conn.commit()

    cur.close()    # Закрываем объект-курсор
    conn.close()    # Закрываем соединение

# ------------------------------------------------------------------------------

num_akk = 0

check_param()
num_akk = get_num_akk()
path_ini = get_path_ini()
os.chdir(path_ini[0])  # смена текущей директории
# print u'Текущая папка:', os.getcwd()

settings = Settings()
users = settings.getUsers()
currentUser = users[num_akk]
settings.setUser(currentUser)
user_id = get_user_id(currentUser)
vk = VK(settings)

print u'Имя аккаунта:', currentUser
print u'user_id:     ', user_id, '\n'

cookies = vk._getSessionCookies()
print 'cookies2', cookies

# name_db = 'cookies.sqlite'  # тестовая база
name_db = get_path_firefox_cookies()
set_cookies_firefox(name_db, cookies)

rstr = [FIREFOX, GAME + user_id]
# subprocess.call(rstr)
subprocess.Popen(rstr)

exit()


# time.sleep(1)
print '-------------   END   ---------------'

# raw_input('-------------   END   ---------------')
# chdir /D C:\Python27\A_Python\SQLite_cookies
# python cookies_sqlite_ff.py

# SELECT id, baseDomain, name, value, host, appId FROM moz_cookies WHERE appId != 0
# SELECT id, baseDomain, name, value, host, isSecure, appId FROM moz_cookies WHERE isSecure != 0 # not like
# SELECT * FROM moz_cookies WHERE host = '.vk.com'
# SELECT * FROM moz_cookies WHERE baseDomain = 'vk.com'


# CREATE TABLE moz_cookies (id INTEGER PRIMARY KEY, baseDomain TEXT, originAttributes TEXT NOT NULL DEFAULT '', name TEXT, value TEXT, host TEXT, path TEXT, expiry INTEGER, lastAccessed INTEGER, creationTime INTEGER, isSecure INTEGER, isHttpOnly INTEGER, appId INTEGER DEFAULT 0, inBrowserElement INTEGER DEFAULT 0, CONSTRAINT moz_uniqueid UNIQUE (name, host, path, originAttributes))



# INSERT INTO classes (Name, RoomNumber) VALUES ('3-А', NULL)
# уникальная пара полей
# CREATE TABLE Follows (from_id INTEGER, to_id INTEGER, UNIQUE(from_id, to_id))
# при вставке ИГНОР если нарушается уникальность
# INSERT OR IGNORE INTO Follows (from_id, to_id) VALUES (7,5)

# cur.rowcount # сколько строк обновилось
# cur.lastrowid  # получить значение id, сгенерированное базой данных для созданной строки

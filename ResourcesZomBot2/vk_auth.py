#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import sys
import time
import json
# sys.path.append(os.path.abspath('..\\API'))
import colorprint as cp
# import vkontakte.api
import requests.api
from sett import Settings
# from game_state.connection import Connection
# from game_state.game_types import GameSTART, GameInfo
# from game_actors_and_handlers.active_user import FinalReportUserVK


class VK():
    def __init__(self, credentials):
        self._credentials = credentials
        self.currentUser = self._credentials.getCurUser()
        self.base_url = 'http://vk.com/'
        self.base_url_s = 'https://vk.com/'
        self.app_id = '612925'  # 'game_zombie_farm'
        self.game_url = 'http://java.shadowlands.ru/zombievk/go'
        self.user_id = None
        self.__game_auth_key = None
        self.__api_access_token = None
        self.user_info = None
        if not os.path.isdir('.\\cookies'): os.makedirs('.\\cookies')
        self.filename_auth = '.\\cookies\\' + self.currentUser + '_auth_and_token.txt'
        self._read_auth()

    def get_game_params(self):
        # print 'user_id      %s' % self.user_id
        # print 'auth_key     %s' % self.__game_auth_key
        # print 'access_token %s' % self.__api_access_token
        if not self._get_user_info():
            params = self.getAppParams()
            self.user_id = params['viewer_id']
            self.__game_auth_key = params['auth_key']
            self.__api_access_token = params['access_token']
            self._save_auth_key_general()
            self._save_auth()
            # print 'self.user_id', self.user_id
            # print 'auth_key', self.__game_auth_key
            # print 'access_token'
            # print self.__api_access_token
        else:
            cp.cprint(u'2Работаем по auth_key и access_token')
        # connection = Connection(self.game_url)
        # return (self.user_id, self.__game_auth_key, self.__api_access_token, connection)

    def getAppParams(self):
        session_cookies = self._getSessionCookies()
        cp.cprint(u'5Получаем новые параметры игры')
        try:
            token_code = requests.get(self.base_url_s + 'app' + self.app_id, cookies=session_cookies)
        except requests.exceptions.TooManyRedirects:
            # cp.cprint(u'12TooManyRedirects')
            self._error_load('game')
        html = token_code.content
        if html:
            params = None
            matcher = re.compile('.*var params = (.*);$')
            for line in html.split('\n'):
                match = matcher.match(line)
                if match is None: continue
                params = match.group(1)
                break
            if params is not None:
                try:
                    return json.loads(params)
                except:
                    self._error_load('game')
        else: self._error_load('game')
        return params

    def _error_load(self, what):
        if what == 'game':
            cp.cprint(u'12Не можем подключиться к игре!')
        elif what == 'vk':
            cp.cprint(u'12Не можем авторизоваться в ВКонтакте!')
        elif what == 'token':
            cp.cprint(u'12Не рабочий токен приложения! Возможно он выдан на другой IP')
            cp.cprint(u'12Пробуем удалить сохранённые данные для последующей  РЕавторизации')
            if os.path.isfile(self.filename_auth):
                os.remove(self.filename_auth)
            file_cook = '.\\cookies\\' + self.currentUser + '.txt'
            if os.path.isfile(file_cook):
                os.remove(file_cook)
        else:
            cp.cprint(u'12Ошибка подключения!')
        # raw_input('-------------   END   ---------------')
        time.sleep(3)
        exit(1)

    def get_time_key(self):
        return None

    # def create_start_command(self, server_time, client_time):
        # if not self.user_info:
            # time.sleep(0.35)
            # self._get_user_info()
            # if not self.user_info:
                # self._error_load('token')
        # command = GameSTART(lang=u'en', info=GameInfo(**self.user_info), 
                # ad=u'user_apps', serverTime=server_time, clientTime=client_time)
        # friendsid = self._get_friends_id()
        # return command, friendsid

    def _read_auth(self):
        if os.path.isfile(self.filename_auth):
            with open(self.filename_auth, 'r') as fr:
                text = fr.read().decode('utf-8')
            if text:
                auth_dict = self.str2dict(text)
                self.user_id = auth_dict.get('user_id', None)
                self.__game_auth_key = auth_dict.get('auth_key', None)
                self.__api_access_token = auth_dict.get('access_token', None)

    def _save_auth(self):
        if self.user_id and self.__game_auth_key and self.__api_access_token:
            auth_list = [u'user_id=' + str(self.user_id)]
            auth_list.append(u'auth_key=' + self.__game_auth_key)
            auth_list.append(u'access_token=' + self.__api_access_token)
            text = u'\n'.join(auth_list)
            with open(self.filename_auth, 'w') as fr:
                fr.write(text.encode('utf-8'))

    def _reading(self, file):
        if os.path.isfile(file):
            with open(file, 'r') as f:
                data = f.read().decode('utf-8')
            spisok = data.split('\n')
            if spisok.count('') > 0:
                spisok.remove('')
        else:
            spisok = []
        return spisok

    def str2dict(self, val):
        if not ((type(val) is str) or (type(val) is unicode)): return val
        res = {tmp.split('=')[0]: tmp.split('=')[1] for tmp in val.replace(' ','').split('\n') if tmp}
        return res

    def _get_user_info(self):
        if not self.__api_access_token: return False
        api = vkontakte.api.API(token=self.__api_access_token)
        fields = 'bdate, sex, first_name, last_name, city, country'
        try:
            info = api.getProfiles(uids=self.user_id, format='json', fields=fields)
            time.sleep(0.35)
            info = info[0]
        except:
            # print u'Не загрузилось'
            return False
        my_country = api.places.getCountryById(cids=int(info['country']))[0]
        time.sleep(0.35)
        info['country'] = my_country['name']

        try:
            my_city = api.places.getCityById(cids=int(info['city']))[0]
            time.sleep(0.35)
            info['city'] = my_city['name']
        except:
            info['city'] = u'Bobruysk'
        self.user_info = dict(city=info['city'],
                        first_name=info['first_name'],
                        last_name=info['last_name'],
                        uid=long(info['uid']),
                        country=info['country'],
                        sex=long(info['sex']),
                        bdate=info.get('bdate', None)
                        )
        return True

    def _get_friends_id(self):
        time.sleep(0.35)
        api = vkontakte.api.API(token=self.__api_access_token)
        info = api.friends.getAppUsers(format='json')
        print u'Всего друзей в списке: %d' % len(info)
        return info

    def _get_friends_all_VK(self, friendsid):  # All VK
        time.sleep(0.35)
        friends_all_id = {}
        fields = 'uid, first_name, last_name, nickname, sex, domain'
        api = vkontakte.api.API(token=self.__api_access_token, fields=fields)
        #, fields=uid, first_name, last_name, nickname, sex, bdate (birthdate), city, country, timezone, photo, photo_medium, photo_big, domain, has_mobile, rate, contacts, education
        info = api.friends.get(format='json', timeout=8)
        # print info
        if len(info) == 5000:
            sd = 1 
            while True:
                api = vkontakte.api.API(token=self.__api_access_token,
                                        offset=5000*sd, fields=fields)
                info2 = api.friends.get(format='json', timeout=8)
                info.extend(info2)
                sd += 1
                if len(info2) < 5000:
                    break
                else:
                    time.sleep(0.34)
        print u'Frends all', len(info)
        print u'Frends ZF', len(friendsid)
        for us in info:
            nickname = ''
            ZF = ''
            uid = us['uid']
            # link = self.base_url_s + 'id' + us['uid']
            link = self.base_url_s + us['domain']
            if hasattr(us,'nickname'): nickname = self._correct(us['nickname'])
            if friendsid.count(uid) > 0: ZF = 'ZF'

            new = {u'link':link, u'first_name':self._correct(us['first_name']), u'last_name':self._correct(us['last_name']), u'nick':nickname, u'app_installed':ZF}
            friends_all_id[uid] = new
        # print (u'Друзей в VK: %s'%str(len(friends_all_id))).encode('cp866')
        with open('friends_all_infoVK.txt', 'w') as f:
            # f.write(unicode(friends_all_id))
            text = json.dumps(friends_all_id, ensure_ascii=False) # simplejson
            f.write(text.encode('UTF-8', 'ignore'))
        # return friends_all_id
        pass

    def _correct(self, name_):
        while ('{' in name_ or
                '}' in name_ or
                '[' in name_ or
                ']' in name_ or
                '^' in name_):
            for l in '{}[]^':
                name_ = name_.replace(l, '')
        if '\u0456' in name_:
            name_ = name_.replace('\u0456', u'i')
        return name_

    def get_cookies_file(self):
        # print u'Текущая папка2:', os.getcwd()
        filename = '.\\cookies\\' + self.currentUser + '.txt'
        if not os.path.isfile(filename): return None
        with open(filename, 'r') as fr:
            text = fr.read().decode('utf-8')
            return self.str2dict(text)

    def set_cookies_file(self, cookies_string):
        if not cookies_string: return
        filename = '.\\cookies\\' + self.currentUser + '.txt'
        with open(filename, 'w') as f:
            f.write(cookies_string.encode('utf-8'))

    def _validateSessionCookies(self, session_cookies):
        if session_cookies is None: return False
        base = requests.get(self.base_url_s, cookies=session_cookies, allow_redirects=False)
        location = base.headers.get('Location', None)
        # print 'status_code', base.status_code
        # print '\nheaders', base.headers
        # print '\ncookies1:', base.cookies.get_dict()
        # print 'location', location
        if base.status_code == 302 and location == '/feed':
            return True
        return False

    def _getSessionCookies(self):
        session_cookies = self.get_cookies_file()
        print 'cookies1', session_cookies
        cookies_are_valid = self._validateSessionCookies(session_cookies)
        if cookies_are_valid: color = 3
        else: color = 12
        cp.cprint(u'9Проверяем cookies: ^%d_%s' % (color, cookies_are_valid))
        if cookies_are_valid: return session_cookies
        cp.cprint(u'5Куки не подходят. Авторизация...')
        with requests.Session() as s: # s = requests.Session()
            base = s.get(self.base_url)  #+ 'login.php'
            matcher = re.compile(r'<input type="hidden" name="([^"]+)" value="([^"]*)" />')
            post = dict(matcher.findall(base._content))
            # print '\ncookies1s:', s.cookies

            username = self._credentials.getUserEmail()
            password = self._credentials.getUserPassword()
            action = 'https://login.vk.com/'
            params = {'act':'login'}
            post.update({'email': username,
                        'pass': password,
                        'expire': '',
                        'captcha_sid': '',
                        'captcha_key': ''
                        })
            login_vk = s.post(action, data=post, params=params) # , allow_redirects=False
            # print login_vk.status_code
            # print '\ncookies2:', login_vk.cookies
            # print '\ncookies2s:', s.cookies
            session_cookies = s.cookies.get_dict(domain='.vk.com')
            # print session_cookies

            cookies_list = []
            for key, val in session_cookies.items():
                cookies_list.append(key + u'=' + val)
            cookies_str = u'\n'.join(cookies_list)
            self.set_cookies_file(cookies_str)
        if not self._validateSessionCookies(session_cookies):
            self._error_load('vk')
        return session_cookies

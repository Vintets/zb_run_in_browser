#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=access_9196.ico
#AutoIt3Wrapper_Res_Fileversion=1.0.0
#AutoIt3Wrapper_Res_LegalCopyright=Vint
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;===============================================================================
;
; Description:      GUI ��� �������� ������� � �������� ��������� �� settings.ini
; Name              Run in browser
; Version:          1.0.0
; Requirement(s):   Autoit 3.3.8.1
; Author(s):        Vint
;
;===============================================================================

#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <FontConstants.au3>
#include <WinAPI.au3>
#include <Array.au3>


Global $aAkkaunts, $akkdata[4]
Local $filename

$filename = @ScriptDir & '\settings.ini'

_ReadSection($filename)
_GUI($filename)
Exit

Func _GUI($filename)
	Local $title, $count, $text_out, $row_text = 1, $num = 0, $akk
	Local $hGUI, $htext, $winX = 860, $winY, $row, $col, $kn = 80, $ots = 20, $knX, $knY
	Local $abrowser[4][2] = [['opera','\Opera\opera.exe'],['chrome','\Google\Chrome\Application\chrome.exe'],['firefox','\Mozilla Firefox\firefox.exe'],['IE','\Google\Chrome\Application\chrome.exe']]
	Local $br[UBound($abrowser)], $Nbr = 0

	$title = '�������� ���� ��������� �� ����� Settings.ini'
	$text_out = '��������� � �������� �������...'
	$count = $aAkkaunts[0]
	Local $hButton[$count]

	Switch $count
		Case 0
			Return
		Case 1 To 5
			$row = 1
			$col = $count
			$winX = 500
		Case 6 To 10
			$row = 2
			$col = 5
			$winX = 500
		Case Else
			If ($count/10) - Int($count/10) = 0 Then
				$row = Int($count/10)
			Else
				$row = Int($count/10) + 1
			EndIf
			;MsgBox(4096, "", "$row " & $row, 3)
			$col = 10
	EndSwitch

	$winY = 125 + $row_text * 20 + ($row-1)*50

	$hGUI = GUICreate($title, $winX, $winY, -1, -1, BitOR($WS_CAPTION, $WS_SYSMENU, $WS_POPUP), $WS_EX_TOPMOST)

	If $row_text > 0 Then
		$htext = GUICtrlCreateLabel($text_out, 50, 32, $winX-100, $row_text * 20, $SS_CENTER)
	EndIf


	For $r = 0 To $row-1
		For $c = 0 To $col-1
			$num += 1
			If $num > $count Then ExitLoop(2)
;~ 			MsgBox(4096, "", "����� " & $num-1, 1)
			$knX = (($winX - $ots*2 - $kn)/($col-1) * $c) + $ots     ; ($winX-10)/($col-1) * ($c+1)
			$knY = (50 + $row_text*20) + $r*50
			$hButton[$num-1] = GUICtrlCreateButton($aAkkaunts[$num], $knX, $knY, $kn, 25, BitOR($BS_CENTER, $BS_VCENTER))
		Next
	Next

	GUIStartGroup()
	For $i = 0 To UBound($abrowser) - 1
		$br[$i] = GUICtrlCreateRadio($abrowser[$i][0], ($winX-50*2-60) / (UBound($abrowser)-1) * $i + 50, $winY-35, 60, 25)
		GUICtrlSetFont(-1, 8, 800, 0, "MS Sans Serif")
	Next
	GUICtrlSetState($br[0], $GUI_CHECKED)

	;~ $brawser = '"' & @ProgramFilesDir & '\Opera\opera.exe"'							; opera
	;~ $brawser = '"' & @ProgramFilesDir & '\Google\Chrome\Application\chrome.exe"'		; chrome
	;~ $brawser = '"' & @ProgramFilesDir & '\Mozilla Firefox\firefox.exe"'				; firefox
	;~ $brawser = '"' & @ProgramFilesDir & '\Google\Chrome\Application\chrome.exe"'		; IE

	GUISetState(@SW_SHOW, $hGUI)

	While 1
		$nMsg = GUIGetMsg()
		If $nMsg = $GUI_EVENT_CLOSE Then ExitLoop

		For $i = 0 To UBound($abrowser) - 1
			If $nMsg = $br[$i] Then $Nbr = $i
		Next

		For $akk = 1 To $count
			If $nMsg = $hButton[$akk-1] Then
				MsgBox(4096, "", "������ ����� " & $akk, 2)
				$brawser = '"' & @ProgramFilesDir & $abrowser[$Nbr][1] & '"'
				_Run($filename, $brawser, $akk)
				Sleep(1000)
			EndIf
		Next
	WEnd
EndFunc

Func _Run($filename, $brawser, $akk)
	Local $param

	_ReadINI($filename, $akk)
	$param = _Param()
	;MsgBox(4096, "", "�������� �������: " &@LF& $param)
	If $param <> '' Then Run($brawser & ' ' & $param)
EndFunc

Func _Param()
	Local $temp, $log, $pass

	If $akkdata[1] = 'mr' Then
		$temp = StringSplit ($akkdata[3], '@', 2)
		$log = $temp[0]
		$domen = $temp[1]
		$pass = $akkdata[2]
		;MsgBox(4096, "", 'akkauntname: ' & $akkdata[0] &@LF& 'mail: ' & $akkdata[3] &@LF& 'login: ' & $log &@LF& 'domen: ' & $domen &@LF& 'pass: ' & $pass)
		return 'win.mail.ru/cgi-bin/auth?page=http://my.mail.ru/apps/609744?ref=promo&post=&Login=' & $log & '&Domain=' & $domen & '&Password=' & $pass & '&level=0'
	ElseIf $akkdata[1] = 'vk' Then
		return ''
	Else
		return ''
	EndIf
EndFunc

Func _ReadINI($filename, $akk)
	$akkdata[0] = $aAkkaunts[$akk]														; akkauntname
	$akkdata[1] = IniRead ( $filename, $akkdata[0], "site", "mr" )						; site
	$akkdata[2] = IniRead ( $filename, $akkdata[0], "user_password", "password" )		; user_password
	$akkdata[3] = IniRead ( $filename, $akkdata[0], "user_email", "email@mail.ru" )		; user_email
EndFunc

Func _ReadSection($filename)
	Dim $aAkkaunts

	$aAkkaunts = IniReadSectionNames($filename)
	If @error Then
		MsgBox(4096, "", "��������� ������, �������� ����������� INI-����.")
		Exit
	EndIf

	For $i = 1 To $aAkkaunts[0]
		If $aAkkaunts[$i] = 'global_settings' And $aAkkaunts[0] > 1 Then
			_ArrayDelete($aAkkaunts, $i)
			$aAkkaunts[0] = $aAkkaunts[0] - 1
			ExitLoop
		EndIf
	Next

	; ���� ��� ���������
	If $aAkkaunts[0] = 0 Then
		MsgBox(4096, "", "� INI-����� ��� ���������")
		Exit
	EndIf
	;_ArrayDisplay( $aAkkaunts, "����� ������" )
EndFunc


;~ $brawser = '"' & @ProgramFilesDir & '\Opera\opera.exe"'							; opera
;~ $brawser = '"' & @ProgramFilesDir & '\Google\Chrome\Application\chrome.exe"'		; chrome
;~ $brawser = '"' & @ProgramFilesDir & '\Mozilla Firefox\firefox.exe"'				; firefox
;~ $brawser = '"' & @ProgramFilesDir & '\Google\Chrome\Application\chrome.exe"'		; IE


#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=access_9196.ico
#AutoIt3Wrapper_Res_Fileversion=1.4.2.0
#AutoIt3Wrapper_Res_LegalCopyright=Vint
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;===============================================================================
;
; Description:      GUI ��� �������� ������� � �������� ��������� �� settings.ini
; Name              Run in browser
; Version:          1.4.2
; Requirement(s):   Autoit 3.3.8.1
; Author(s):        Vint
;
;===============================================================================

#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <FontConstants.au3>
#include <WinAPI.au3>
#include <GUIScrollBars.au3>
#include <ScrollBarConstants.au3>
#include <Array.au3>


Const $Version = '1.4.2'
Global $aAkkaunts, $akkdata[1][14]
Global $RunData[4], $hGUIRun, $hGUIRunCh, $browser_like = 1, $winX = 877, $messX = 30
Local $filename

$filename = @ScriptDir & '\settings.ini'
Opt('GUIResizeMode', $GUI_DOCKALL)

_ReadingINI($filename)
_RunningAs($filename)
_EndProgramm()

Func _RunningAs($filename)
	Local $count, $num = 0, $akk, $bot_state
	Local $widthBO = 860, $winY, $row, $col, $kn = 80, $ots = 20, $knX, $knY, $brow, $bot, $close_cmd
	Local $abrowser[4][2] = [['opera','\Opera\opera.exe'],['chrome','\Google\Chrome\Application\chrome.exe'],['firefox','\Mozilla Firefox\firefox.exe'],['IE','\Internet Explorer\iexplore.exe']]
	Local $br[UBound($abrowser)], $Nbr = $browser_like, $bot_state = False, $cmd = True
	Local $sm1X = 0, $LenNameBr, $iWinSize, $hMessage, $h_str, $brawser
	Local $title = '������ ���� ��������� �� ����� Settings.ini'

	If FileExists(@ProgramFilesDir & '\Mozilla Firefox\firefox.exe') Then
	Else
		If FileExists(@ProgramFilesDir & '\Firefox Developer Edition\firefox.exe') Then
			$abrowser[2][1] = '\Firefox Developer Edition\firefox.exe'
		EndIf
	EndIf

	$count = $aAkkaunts[0] - 1
	Local $hButton[$count]

	Switch $count
		Case 0
			Return
		Case 1 To 5
			$row = 1
			$col = $count
			$winX = 500
			$widthBO = 500
		Case 6 To 10
			$row = 2
			$col = 5
			$winX = 500
			$widthBO = 500
		Case Else
			If Mod($count, 10) = 0 Then
				$row = Int($count/10)
			Else
				$row = Int($count/10) + 1
			EndIf
			;MsgBox(4096, '', '$row ' & $row, 3)
			$col = 10
			$sm1X = 20
	EndSwitch

	$winY = 150 + ($row-1)*50 
	$h_str = Ceiling($winY / 20)
	$winY += $messX
	;ConsoleWrite('$winY ' & $winY & '  $h_str ' & $h_str & @CRLF)

	$hGUIRun = GUICreate($title, $winX, $winY, -1, -1, BitOR($WS_OVERLAPPEDWINDOW, $WS_SIZEBOX, $WS_POPUP)) ; , $WS_EX_TOPMOST

	;GUIStartGroup($hGUIRun)
	GUICtrlCreateGroup('', (20 + $sm1X), 5, 180, 65)
	$bot = GUICtrlCreateRadio('��������� � Zombot', (35 + $sm1X), 15, 150, 20)
	GUICtrlSetFont(-1, 8, 800, 0, 'MS Sans Serif')
	$brow = GUICtrlCreateRadio('��������� � ��������', (35 + $sm1X), 45, 150, 20)
	GUICtrlSetFont(-1, 8, 800, 0, 'MS Sans Serif')
	GUICtrlSetState($brow, $GUI_CHECKED)
	GUICtrlCreateGroup('', -99, -99, 1, 1)

	$close_cmd = GUICtrlCreateCheckbox('��������� �������', (230 + $sm1X), 17, 120, 17)
	GUICtrlSetState($close_cmd, $GUI_CHECKED)
	GUICtrlSetState($close_cmd,$GUI_DISABLE)

	;GUIStartGroup()
	GUICtrlCreateGroup('', (220 + $sm1X), 35, 260, 35)
	For $i = 0 To UBound($abrowser) - 1
		If StringLen($abrowser[$i][0]) < 4 Then
			$LenNameBr = 35
		Else
			$LenNameBr = 60
		EndIf
		$br[$i] = GUICtrlCreateRadio($abrowser[$i][0], (230 + $sm1X) + ($i * 70), 45, $LenNameBr, 20)
		;GUICtrlSetBkColor(-1, 0xDEE1B4)
		GUICtrlSetFont(-1, 8, 800, 0, 'MS Sans Serif')
	Next
	GUICtrlCreateGroup('', -99, -99, 1, 1)
	GUICtrlSetState($br[$browser_like], $GUI_CHECKED)

	$hMessage = GUICtrlCreateLabel('', 60, $winY-25, 350, 15)
	GUICtrlSetColor(-1, 0x3030FF)
	;GUICtrlSetBkColor(-1, 0xFF0000)
	;GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
	GUICtrlSetResizing($hMessage, $GUI_DOCKSIZE + $GUI_DOCKLEFT + $GUI_DOCKBOTTOM)

	$iWinSize = WinGetClientSize($hGUIRun)
	; ���� ������ (�����)
	$hGUIRunCh = GUICreate('', $winX - 5, ($iWinSize[1] - 85 - $messX), 0, 80, $WS_CHILD, $WS_EX_CLIENTEDGE, $hGUIRun)
	GUICtrlSetResizing($hGUIRunCh, $GUI_DOCKALL)
	GUISetBkColor(0xF5F5F5)
	If $count > 10 Then
		_GUIScrollBars_Init($hGUIRunCh, $winX - 38, $h_str) ;10
		_GUIScrollBars_ShowScrollBar($hGUIRun, $SB_VERT, False)
	EndIf

	If $col <> 1 Then
		For $r = 0 To $row-1
			For $c = 0 To $col-1
				If $num > $count - 1 Then ExitLoop(2)
	 			;MsgBox(4096, '', '����� ' & $num, 1)
				$knX = (($widthBO - $ots*2 - $kn)/($col-1) * $c) + $ots
				$knY = $r*50 + 20
				GUICtrlCreateLabel($num+1, $knX + 25, $knY - 13, 30, 12, $SS_CENTER)
; 				GUICtrlSetFont(-1, 8, 800, 0, 'MS Sans Serif')
				GUICtrlSetColor(-1, 0x9090FF)
				;GUICtrlSetBkColor(-1, 0xFFCCCC)
				$hButton[$num] = GUICtrlCreateButton($aAkkaunts[$num + 2], $knX, $knY, $kn, 25, BitOR($BS_CENTER, $BS_VCENTER))
				$num += 1
			Next
		Next
	ElseIf $col = 1 Then
		$knX = ($widthBO/2 - $kn/2)
		$knY = 20
		$hButton[0] = GUICtrlCreateButton($aAkkaunts[$num + 2], $knX, $knY, $kn, 25, BitOR($BS_CENTER, $BS_VCENTER))
	EndIf

	GUIRegisterMsg(0x0024, 'WM_GETMINMAXINFO') ; ������������ ��������
	GUIRegisterMsg($WM_SIZE, 'WM_SIZE')
	GUIRegisterMsg($WM_VSCROLL, 'WM_VSCROLL')
	GUIRegisterMsg($WM_HSCROLL, 'WM_HSCROLL')

	GUISwitch($hGUIRun)
	GUISetState(@SW_SHOW, $hGUIRunCh)
	GUISetState(@SW_SHOW, $hGUIRun)

	While 1
		$nMsg = GUIGetMsg()
		Select
			Case $nMsg = 0
			Case $nMsg = $GUI_EVENT_CLOSE
				ExitLoop
			Case $nMsg = $GUI_EVENT_RESIZED Or $nMsg = $GUI_EVENT_MAXIMIZE Or $nMsg = $GUI_EVENT_RESTORE
				_SubGUIResized2()
			Case $nMsg = $brow
				$bot_state = False
				For $i = 0 To UBound($abrowser) - 1
					GUICtrlSetState($br[$i],$GUI_ENABLE)
				Next
				GUICtrlSetState($close_cmd,$GUI_DISABLE)
			Case $nMsg = $bot
				$bot_state = True
				For $i = 0 To UBound($abrowser) - 1
					GUICtrlSetState($br[$i],$GUI_DISABLE)
				Next
				GUICtrlSetState($close_cmd,$GUI_ENABLE)
			Case $nMsg = $close_cmd
				If BitAnd(GUICtrlRead($close_cmd),$GUI_CHECKED) Then
					$cmd = True
				Else
					$cmd = False
				EndIf
		EndSelect

		For $i = 0 To UBound($abrowser) - 1
			If $nMsg = $br[$i] Then $Nbr = $i
		Next

		For $akk = 0 To $count - 1
			If $nMsg = $hButton[$akk] Then
				;ConsoleWrite($nMsg & ' ����' & @CRLF)
				;ContinueLoop
				$brawser = '"' & @ProgramFilesDir & $abrowser[$Nbr][1] & '"'
				_Run($brawser, $akk, $bot_state, $cmd, $hMessage)
				Sleep(1000)
			EndIf
		Next
		sleep(20)
	WEnd
	GUIDelete($hGUIRun)
EndFunc   ;==>_RunningAs

Func _Run($brawser, $akk, $bot_state, $cmd, $hMessage)
	Local $param, $text_message

	_SetDataRun($akk)
	If Not $bot_state Then $param = _Param()

	$text_message = '������� � ' & $akk + 1 & ',   ��� ��������:  ' & $RunData[0]
	GUICtrlSetData($hMessage, $text_message)
	;MsgBox(4096, '', '������ ����� ' & $akk, 2)
	;MsgBox(4096, '', '�������� �������: ' &@LF& $param)

	If $bot_state Then
		If $cmd Then
			Run('cmd.exe /C path=C:\Python27;%PATH% && cd '&@ScriptDir&' && Start Python main.py -c ' & $akk)
		Else
			Run('cmd.exe /K path=C:\Python27;%PATH% && cd '&@ScriptDir&' && Python main.py -c ' & $akk)
		EndIf
	Else
		If $param <> '' Then Run($brawser & ' ' & $param)
	EndIf
EndFunc

Func _Param()
	Local $temp, $log, $pass, $domen

	If $RunData[1] = 'mr' Then
		$temp = StringSplit ($RunData[3], '@', 2)
		$log = $temp[0]
		$domen = $temp[1]
		$pass = $RunData[2]
		;MsgBox(4096, '', 'akkauntname: ' & $RunData[0] &@LF& 'mail: ' & $RunData[3] &@LF& 'login: ' & $log &@LF& 'domen: ' & $domen &@LF& 'pass: ' & $pass)
		return 'win.mail.ru/cgi-bin/auth?page=http://my.mail.ru/apps/609744?ref=promo&post=&Login=' & $log & '&Domain=' & $domen & '&Password=' & $pass & '&level=0'
	ElseIf $RunData[1] = 'vk' Then
		MsgBox(0x1030, '',  '����...' & @CRLF & @CRLF & '���������� � �������� �������� ������ � mail.ru' & @CRLF & 'VK ������� ����, � ��� ��������� �� �����.', 0, $hGUIRun)
		return ''
		$log = $RunData[3]
		$pass = $RunData[2]
		MsgBox(4096, '', 'akkauntname: ' & $RunData[0] &@LF& 'mail: ' & $RunData[3] &@LF& 'login: ' & $log &@LF& 'pass: ' & $pass)
		return 'login.vk.com/?act=login&q=1&al_frame=1&from_host=vk.com&email=' & $log & '&pass=' & $pass
	Else
		return ''
	EndIf
EndFunc

Func _SetDataRun($akk)
	$RunData[0] = $aAkkaunts[$akk]	; akkauntname
	$RunData[1] = $akkdata[$akk][1]	; site
	$RunData[2] = $akkdata[$akk][3]	; user_password
	$RunData[3] = $akkdata[$akk][2]	; user_email
EndFunc

Func _ReadingINI($filename)
	Dim $aAkkaunts

	$aAkkaunts = IniReadSectionNames($filename)
	If @error Then
		MsgBox(4096, '', '��������� ������, �������� ����������� INI-����.')
		_EndProgramm()
	EndIf

; 	For $i = 1 To $aAkkaunts[0]
; 		If $aAkkaunts[$i] = 'global_settings' And $aAkkaunts[0] > 1 Then
; 			_ArrayDelete($aAkkaunts, $i)
; 			$aAkkaunts[0] = $aAkkaunts[0] - 1
; 			ExitLoop
; 		EndIf
; 	Next

	; ���� ��� ���������
	If $aAkkaunts[0] = 0 Or ($aAkkaunts[0] = 1 And $aAkkaunts[1] = 'global_settings') Then
		MsgBox(4096, '', '� INI-����� ��� ���������' & @CRLF & '������ ��������', 5)
		_EndProgramm()
	EndIf
	;_ArrayDisplay( $aAkkaunts, '����� ������' )
	
	Global $akkdata[$aAkkaunts[0] - 1][14]
	
	For $akksort = 2 To $aAkkaunts[0]
		$akkauntname = $aAkkaunts[$akksort]
		$site = IniRead ( $filename, $akkauntname, 'site', 'mr' )
		$user_email = IniRead ( $filename, $akkauntname, 'user_email', 'email@mail.ru' )
		$user_password = IniRead ( $filename, $akkauntname, 'user_password', 'password' )
		$setting_view = IniRead ( $filename, $akkauntname, 'setting_view', "{'pickup':True,'location_send':True}" )
		$seed_item = IniRead ( $filename, $akkauntname, 'seed_item', '' )
		$cook_item = IniRead ( $filename, $akkauntname, 'cook_item', '' )
		$locations_only = IniRead ( $filename, $akkauntname, 'locations_only', '[]' )
		$locations_nfree = IniRead ( $filename, $akkauntname, 'locations_nfree', "[u'isle_01',u'isle_small',u'isle_star',u'isle_large',u'isle_moon',u'isle_giant',u'isle_xxl',u'isle_desert']" )
		$locations_nwalk = IniRead ( $filename, $akkauntname, 'locations_nwalk', "[u'un_0'+str(x+1) for x in range(9)]" )
		$sell_item = IniRead ( $filename, $akkauntname, 'sell_item', '{}' )
		$locations_nother = IniRead ( $filename, $akkauntname, 'locations_nother', '[]' )
		$send_user = IniRead ( $filename, $akkauntname, 'send_user', '' )
		$dig_friends = IniRead ( $filename, $akkauntname, 'dig_friends', '[]' )

		$akkdata[$akksort-2][0] = $akkauntname
		$akkdata[$akksort-2][1] = $site
		$akkdata[$akksort-2][2] = $user_email
		$akkdata[$akksort-2][3] = $user_password
		$akkdata[$akksort-2][4] = $setting_view
		$akkdata[$akksort-2][5] = $seed_item
		$akkdata[$akksort-2][6] = $cook_item
		$akkdata[$akksort-2][7] = $locations_only
		$akkdata[$akksort-2][8] = $locations_nfree
		$akkdata[$akksort-2][9] = $locations_nwalk
		$akkdata[$akksort-2][10] = $sell_item
		$akkdata[$akksort-2][11] = $locations_nother
		$akkdata[$akksort-2][12] = $send_user
		$akkdata[$akksort-2][13] = $dig_friends
	Next
EndFunc

Func _SubGUIResized2()
	Local $iWinSize

	$iWinSize = WinGetClientSize($hGUIRun)
	WinMove($hGUIRunCh, '', 0, 80, $iWinSize[0] - 1, ($iWinSize[1] - 81 - $messX))
EndFunc   ;==>_SubGUIResized2

Func _EndProgramm()
	Local $FormEnd, $i, $hParent, $SoundCancel

	$hParent  = GUICreate('')
	$FormEnd = GUICreate('� ���������', 200, 160, -1, -1, BitOR($WS_DISABLED, $WS_POPUP), $WS_EX_TOPMOST, $hParent)
; 	GUICtrlCreatePic(@ScriptDir & '\ResourcesZomBot2\Vint_avatar_11_64x64.jpg', 68, 8, 64, 64)
	GUICtrlCreateLabel('Autor Vint', 68, 80, 70, 20)
	GUICtrlSetFont(-1, 10, 800, 0, 'MS Sans Serif')
	GUICtrlSetColor(-1, 0x800080)
	GUICtrlCreateLabel('version ' & $Version & ' of 25.05.2016', 45, 95, 125, 17)
	GUICtrlSetFont(-1, 8, 400, 0, 'Tahoma')
	GUICtrlCreateLabel('���������� ���', 60, 116, 100, 17)
	GUICtrlCreateLabel('Zombot_Vint_9.8++', 62, 130, 82, 17)
	WinSetTrans($FormEnd, '', 0) ; ������������ 30
	GUISetBkColor(0xDCF2CA, $FormEnd)
	GUISetState(@SW_SHOW)

	For $i = 0 To 255 Step 8
		WinSetTrans($FormEnd, '', $i)
		Sleep(1)
	Next
	$i = 1224
	While $i >= 0
		If $i < 256 Then WinSetTrans($FormEnd, '', $i)
		Sleep(1)
		$i -= 8
	WEnd
	Exit
EndFunc   ;==>_EndProgramm

Func WM_GETMINMAXINFO($hWnd, $iMsg, $wParam, $lParam)
	Local $k, $iWinSize
	$k += 1
	#forceref $iMsg, $wParam
	If  $hWnd = $hGUIRun Then
		$iWinSize = WinGetPos($hGUIRun)
		Local $tMINMAXINFO = DllStructCreate('int;int;' & _
					'int MaxSizeX; int MaxSizeY;' & _
					'int MaxPositionX;int MaxPositionY;' & _
					'int MinTrackSizeX; int MinTrackSizeY;' & _
					'int MaxTrackSizeX; int MaxTrackSizeY', _
					$lParam)
		DllStructSetData($tMINMAXINFO, 'MinTrackSizeX', 500) ; ����������� ������� ����
		DllStructSetData($tMINMAXINFO, 'MinTrackSizeY', 180 + $messX)
		DllStructSetData($tMINMAXINFO, 'MaxTrackSizeX', $winX + 8) ; ������������ ������� ����
; 		DllStructSetData($tMINMAXINFO, 'MaxTrackSizeY', 180)
		DllStructSetData($tMINMAXINFO, 'MaxSizeX', $winX + 8) ; ������� ����������� ��������� ( ������ ����� ������, ���� ������������ ��������)
		DllStructSetData($tMINMAXINFO, 'MaxSizeY', @DesktopHeight - 30)
; 		DllStructSetData($tMINMAXINFO, 'MaxSizeY', 664)
		DllStructSetData($tMINMAXINFO, 'MaxPositionX', $iWinSize[0]) ; ������� � ���������� ���������
; 		DllStructSetData($tMINMAXINFO, 'MaxPositionY', 450)
	EndIf
EndFunc   ;==>_WM_GETMINMAXINFO

Func WM_SIZE($hWnd, $Msg, $wParam, $lParam)
	#forceref $Msg, $wParam
	Local $index = -1, $yChar, $xChar, $xClientMax, $xClient, $yClient, $ivMax
	Local $iWinSize, $on = False

	For $x = 0 To UBound($aSB_WindowInfo) - 1
		If $aSB_WindowInfo[$x][0] = $hWnd Then
			$on = True
			ExitLoop
		EndIf
	Next	
	If Not $on Then Return $GUI_RUNDEFMSG
	;ConsoleWrite($aSB_WindowInfo[0][0] & '  ' & $hWnd & ' | ' & $hGUIRun & ' ' & $hGUIRunCh & @CRLF)

	For $x = 0 To UBound($aSB_WindowInfo) - 1
		If $aSB_WindowInfo[$x][0] = $hWnd Then
			$index = $x
			$xClientMax = $aSB_WindowInfo[$index][1]
			$xChar = $aSB_WindowInfo[$index][2]
			$yChar = $aSB_WindowInfo[$index][3]
			$ivMax = $aSB_WindowInfo[$index][7]
			ExitLoop
		EndIf
	Next
	If $index = -1 Then Return 0

	Local $tSCROLLINFO = DllStructCreate($tagSCROLLINFO)

	; Retrieve the dimensions of the client area.
	$xClient = BitAND($lParam, 0x0000FFFF)
	$yClient = BitShift($lParam, 16)
	$aSB_WindowInfo[$index][4] = $xClient
	$aSB_WindowInfo[$index][5] = $yClient

	; Set the vertical scrolling range and page size
	DllStructSetData($tSCROLLINFO, 'fMask', BitOR($SIF_RANGE, $SIF_PAGE))
	DllStructSetData($tSCROLLINFO, 'nMin', 0)
	DllStructSetData($tSCROLLINFO, 'nMax', $ivMax)
	DllStructSetData($tSCROLLINFO, 'nPage', $yClient / $yChar)
	_GUIScrollBars_SetScrollInfo($hWnd, $SB_VERT, $tSCROLLINFO)

; 	If $hWnd = $hForm1 Then
; 		$iWinSize = WinGetClientSize($hForm1)
; 		If $xClient <> $iWinSize[0] Then $xClient = $iWinSize[0]
; 	EndIf

	; Set the horizontal scrolling range and page size
	DllStructSetData($tSCROLLINFO, 'fMask', BitOR($SIF_RANGE, $SIF_PAGE))
	DllStructSetData($tSCROLLINFO, 'nMin', 0)
	DllStructSetData($tSCROLLINFO, 'nMax', $xClientMax / $xChar)
	DllStructSetData($tSCROLLINFO, 'nPage', $xClient / $xChar)
	_GUIScrollBars_SetScrollInfo($hWnd, $SB_HORZ, $tSCROLLINFO)

; 	ConsoleWrite('$hWnd ' & $hWnd & ',  x/y: ' & $xClient & '/' & $yClient & '  $xClientMax ' & $xClientMax & @CRLF)
; 	_ArrayDisplay($aSB_WindowInfo, '$aSB_WindowInfo')
	Return $GUI_RUNDEFMSG
EndFunc   ;==>WM_SIZE

Func WM_HSCROLL($hWnd, $Msg, $wParam, $lParam)
	#forceref $Msg, $lParam
	Local $nScrollCode = BitAND($wParam, 0x0000FFFF)

	Local $index = -1, $xChar, $xPos
	Local $Min, $Max, $Page, $Pos, $TrackPos

	For $x = 0 To UBound($aSB_WindowInfo) - 1
		If $aSB_WindowInfo[$x][0] = $hWnd Then
			$index = $x
			$xChar = $aSB_WindowInfo[$index][2]
			ExitLoop
		EndIf
	Next
	If $index = -1 Then Return 0

;~  ; Get all the horizontal scroll bar information
	Local $tSCROLLINFO = _GUIScrollBars_GetScrollInfoEx($hWnd, $SB_HORZ)
	$Min = DllStructGetData($tSCROLLINFO, 'nMin')
	$Max = DllStructGetData($tSCROLLINFO, 'nMax')
	$Page = DllStructGetData($tSCROLLINFO, 'nPage')
	; Save the position for comparison later on
	$xPos = DllStructGetData($tSCROLLINFO, 'nPos')
	$Pos = $xPos
	$TrackPos = DllStructGetData($tSCROLLINFO, 'nTrackPos')
	#forceref $Min, $Max
	Switch $nScrollCode
		Case $SB_LINELEFT ; user clicked left arrow
			DllStructSetData($tSCROLLINFO, 'nPos', $Pos - 1)

		Case $SB_LINERIGHT ; user clicked right arrow
			DllStructSetData($tSCROLLINFO, 'nPos', $Pos + 1)

		Case $SB_PAGELEFT ; user clicked the scroll bar shaft left of the scroll box
			DllStructSetData($tSCROLLINFO, 'nPos', $Pos - $Page)

		Case $SB_PAGERIGHT ; user clicked the scroll bar shaft right of the scroll box
			DllStructSetData($tSCROLLINFO, 'nPos', $Pos + $Page)

		Case $SB_THUMBTRACK ; user dragged the scroll box
			DllStructSetData($tSCROLLINFO, 'nPos', $TrackPos)
	EndSwitch

;~    // Set the position and then retrieve it.  Due to adjustments
;~    //   by Windows it may not be the same as the value set.

	DllStructSetData($tSCROLLINFO, 'fMask', $SIF_POS)
	_GUIScrollBars_SetScrollInfo($hWnd, $SB_HORZ, $tSCROLLINFO)
	_GUIScrollBars_GetScrollInfo($hWnd, $SB_HORZ, $tSCROLLINFO)
	;// If the position has changed, scroll the window and update it
	$Pos = DllStructGetData($tSCROLLINFO, 'nPos')
	If ($Pos <> $xPos) Then _GUIScrollBars_ScrollWindow($hWnd, $xChar * ($xPos - $Pos), 0)
	Return $GUI_RUNDEFMSG
EndFunc   ;==>WM_HSCROLL

Func WM_VSCROLL($hWnd, $Msg, $wParam, $lParam)
	#forceref $Msg, $wParam, $lParam
	Local $nScrollCode = BitAND($wParam, 0x0000FFFF)
	Local $index = -1, $yChar, $yPos
	Local $Min, $Max, $Page, $Pos, $TrackPos
	For $x = 0 To UBound($aSB_WindowInfo) - 1
		If $aSB_WindowInfo[$x][0] = $hWnd Then
			$index = $x
			$yChar = $aSB_WindowInfo[$index][3]
			ExitLoop
		EndIf
	Next
	If $index = -1 Then Return 0
	; Get all the vertial scroll bar information
	Local $tSCROLLINFO = _GUIScrollBars_GetScrollInfoEx($hWnd, $SB_VERT)
	$Min = DllStructGetData($tSCROLLINFO, 'nMin')
	$Max = DllStructGetData($tSCROLLINFO, 'nMax')
	$Page = DllStructGetData($tSCROLLINFO, 'nPage')
	; Save the position for comparison later on
	$yPos = DllStructGetData($tSCROLLINFO, 'nPos')
	$Pos = $yPos
	$TrackPos = DllStructGetData($tSCROLLINFO, 'nTrackPos')

	Switch $nScrollCode
		Case $SB_TOP ; user clicked the HOME keyboard key
			DllStructSetData($tSCROLLINFO, 'nPos', $Min)
		Case $SB_BOTTOM ; user clicked the END keyboard key
			DllStructSetData($tSCROLLINFO, 'nPos', $Max)
		Case $SB_LINEUP ; user clicked the top arrow
			DllStructSetData($tSCROLLINFO, 'nPos', $Pos - 1)
		Case $SB_LINEDOWN ; user clicked the bottom arrow
			DllStructSetData($tSCROLLINFO, 'nPos', $Pos + 1)
		Case $SB_PAGEUP ; user clicked the scroll bar shaft above the scroll box
			DllStructSetData($tSCROLLINFO, 'nPos', $Pos - $Page)
		Case $SB_PAGEDOWN ; user clicked the scroll bar shaft below the scroll box
			DllStructSetData($tSCROLLINFO, 'nPos', $Pos + $Page)
		Case $SB_THUMBTRACK ; user dragged the scroll box
			DllStructSetData($tSCROLLINFO, 'nPos', $TrackPos)
	EndSwitch

	DllStructSetData($tSCROLLINFO, 'fMask', $SIF_POS)
	_GUIScrollBars_SetScrollInfo($hWnd, $SB_VERT, $tSCROLLINFO)
	_GUIScrollBars_GetScrollInfo($hWnd, $SB_VERT, $tSCROLLINFO)
	$Pos = DllStructGetData($tSCROLLINFO, 'nPos')

	If ($Pos <> $yPos) Then
		_GUIScrollBars_ScrollWindow($hWnd, 0, $yChar * ($yPos - $Pos))
		$yPos = $Pos
	EndIf
	Return $GUI_RUNDEFMSG
EndFunc   ;==>WM_VSCROLL



; $brawser = '"' & @ProgramFilesDir & '\Opera\opera.exe"'								; opera
; $brawser = '"' & @ProgramFilesDir & '\Google\Chrome\Application\chrome.exe"'	; chrome
; $brawser = '"' & @ProgramFilesDir & '\Mozilla Firefox\firefox.exe"'				; firefox
; $brawser = '"' & @ProgramFilesDir & '\Internet Explorer\iexplore.exe"'			; IE

